# Heroku Link
[`https://what-i-am-going-to-do.herokuapp.com`](https://what-i-am-going-to-do.herokuapp.com)

##### If you dont want waste time for registration, you can use this account:
- login: admin1@gmail.com
- password: admin1

# How to start
### Installing Ruby

- Install Ruby

- To verify that the installation it was successful run the following command which will print the Ruby version

```ruby --version```

- The output will look something like this:

```ruby 2.7.1p83 (2020-03-31 revision a0c7c23c9c) [x86_64-linux]```



### Setting Up PostgreSQL
```sudo apt install postgresql-11 libpq-dev```

- The postgres installation does not setup a user for you, so you will need to follow these steps to create a user with permission to create databases. Feel free to replace chris with your username:

```sudo -u postgres createuser test -s```

- If you would like to set a password for the user, you can do the following

```sudo -u postgres psql```

```postgres=# \password test```

### Clone the repository
 ``` cd folder_name```

```git clone git@gitlab.com:horosen/todo.git ```

### Install dependencies
```bundle install```

### Creating database
`rake db:setup`

### Starting Rails server

```cd folder_name/todo```

```sudo service postgresql start ```

```rails server```

- If you succeed, then now You can now visit http://localhost:3000 to view your website!