class TodoItem < ApplicationRecord
  belongs_to :todo_list
  validates :content, presence: true
  validates :content, length: { minimum: 3 }
  validates :content, uniqueness: true

  def completed?
    completed_at.present?
  end

  def toggle_completion
    return update_attribute(:completed_at, Time.now) unless completed?

    update_attribute(:completed_at, nil)
  end
end
